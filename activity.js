/*Use the count operator to count the total number of
fruits on sale.*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$count: "fruitsOnSale"}
	])


/*Use the count operator to count the total number of
fruits with stock more than 20*/

db.fruits.aggregate([
		{$match: {stock: {$gte: 20}}},
		{$count: "fruitStocks"}
	])


/*Use the average operator ($avg) to get the average
price of fruits onSale per supplier.*/

db.fruits.aggregate([
		{$group: {_id: "$supplier_id", avgPrice: {$avg: "$price"}}}
	])

/*Use the max operator($max) to get the highest price of
a fruit per supplier.*/

db.fruits.aggregate([
		{$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}}
	])

/*Use the min operator ($min) to get the lowest price
of a fruit per supplier.*/

db.fruits.aggregate([
		{$group: {_id: "$supplier_id", minPrice: {$min: "$price"}}}
	])
